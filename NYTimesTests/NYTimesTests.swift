//
//  NYTimesTests.swift
//  NYTimesTests
//
//  Created by Admin on 2/13/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import XCTest
@testable import NYTimes

class NYTimesTests: XCTestCase {
    
    var NewsUnderTest: NewsViewController!
    var NewsSession: URLSession!
    var periods = [1,7,30]

    override func setUp() {
        super.setUp()
        NewsSession = URLSession(configuration: URLSessionConfiguration.default)
    }

    override func tearDown() {
        NewsUnderTest = nil
        NewsSession = nil
        super.tearDown()
    }
    
    // MARK:- My Test Functions
    func testNewsCoreData() {
    }
    
    func testNewsSimpleApiCall() {
        for period in periods {
            // given
            let url = URL(string: "\(coreapi.BaseUrl)\(APIManager.getNewsAPIUrl(period: period))\(coreapi.ApiKey)")
            
            // 1
            let promise = expectation(description: "Completion handler invoked")
            var statusCode: Int?
            var responseError: Error?
            
            // when
            let dataTask = NewsSession.dataTask(with: url!) { data, response, error in
                statusCode = (response as? HTTPURLResponse)?.statusCode
                responseError = error
                // 2
                promise.fulfill()
            }
            dataTask.resume()
            // 3
            waitForExpectations(timeout: 5, handler: nil)
            
            // then
            XCTAssertNil(responseError) //We want the response error to be nil, else, the test should fail
            XCTAssertEqual(statusCode, 200) //We want the response status code to be 200, else, the test should fail
        }
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
