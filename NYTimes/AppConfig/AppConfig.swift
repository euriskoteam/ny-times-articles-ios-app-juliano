//
//  AppConfig.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

//
//  AppConfig.swift
//  NYTimes
//
//  Created by Eurisko on 2/11/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Crashlytics
import Fabric
import UIKit

class AppConfig: NSObject {
    static let shared = AppConfig()
    
    private override init() {
        super.init()
        initDefaultValue()
    }
    
    // Global Vars Across The Entire App ..
    var _launchOptions = [UIApplication.LaunchOptionsKey: Any]()
    
    // Storyboards
    var mainStoryboard = UIStoryboard()
    
    // MARK: - Init The App Default Values
    
    func initDefaultValue() {
        // init Crashlytics
        Fabric.with([Crashlytics.self])
        
        // init Storyboards
        initStoryboards()
        
        // Init Reachability
        initReachabilityAndStartTheApp()
    }
    
    // MARK: - App Main Delegate Functions
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Save the launchOptions for later usage ..
        _launchOptions = launchOptions ?? [UIApplication.LaunchOptionsKey: Any]()
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {}
    
    func applicationDidEnterBackground(_ application: UIApplication) {}
    
    func applicationWillEnterForeground(_ application: UIApplication) {}
    
    func applicationDidBecomeActive(_ application: UIApplication) {}
    
    func applicationWillTerminate(_ application: UIApplication) {}
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {}
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {}
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {}
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        return true
    }
    
    // MARK: - init Storyboards
    
    func initStoryboards() {
        mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    }
    
    // MARK: - Init Reachability & Start the app
    
    func initReachabilityAndStartTheApp() {
        // declare this property where it won't go out of scope relative to your listener
       AppNetworking.init().startNetworkReachabilityObserver()
    }
}
