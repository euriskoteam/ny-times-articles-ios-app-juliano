//
//  Constants.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation
import UIKit

// ******************** Public Usage ********************//

let appDelegate: AppDelegate? = {
    guard let _appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return nil
    }
    return _appDelegate
}()

let isDebug = true
let coreapp = AppConfig.shared
var coreapi = APIConfig.shared
var currentVC: UIViewController!
let api = APIManager.shared

// ******************** End of Public Usage ********************//

public func translate(key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

struct Constants {
    static let APPSTORE_ID = ""
}

// User Default
struct UserDefaultsKey {
    static let didFetchBefore = "didFetchBefore"
}

// Keychain
struct KeychainKeys {}

// In-app Notifications
struct NotificationsKeys {}

struct DateFormat {
    static let global_ui_dateformat_1 = "EE MMM dd yyyy"
}

// UI tags
struct GlobalTags {
    static let PULL_TO_REFRESH_TAG = 00223
}

// ******************** Public Usage ********************//
let appNetworking = AppNetworking.sharedInstance
