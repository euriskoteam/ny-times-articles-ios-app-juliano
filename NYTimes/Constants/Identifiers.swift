//
//  Identifiers.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

struct StoryboardIdentifier {
    static let NewsViewController = "NewsViewController"
    static let NewsDetailsViewController = "NewsDetailsViewController"
}

//struct ViewIdentifier {
//}

struct CellIdentifier {
    static let NewsCollectionViewCell = "NewsCollectionViewCell"
}
