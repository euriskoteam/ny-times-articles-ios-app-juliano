//
//  Fonts.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

struct Font {
    static let names: [String?] = UIFont.fontNames(forFamilyName: "") // Add your font name
    
    static var textLight: UIFont {
        if let fontName = Font.names[0] {
            return UIFont(name: fontName, size: 16)!
        }
        
        return UIFont()
    }
    
    static var textRegular: UIFont {
        if let fontName = Font.names[1] {
            return UIFont(name: fontName, size: 16)!
        }
        
        return UIFont()
    }
    
    static var textBold: UIFont {
        if let fontName = Font.names[2] {
            return UIFont(name: fontName, size: 16)!
        }
        
        return UIFont()
    }
}
