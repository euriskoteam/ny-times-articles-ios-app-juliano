//
//  Enums.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

enum SegueType {
    case push
    case present
}

enum Animation {
    case fadeIn
    case fadeOut
}

enum Side {
    case top
    case bottom
    case right
    case left
}

enum JPEGQuality: CGFloat {
    case lowest = 0.0
    case low = 0.25
    case medium = 0.5
    case high = 0.75
    case highest = 1.0
}

enum Keys: String {
    case accessToken = "Access-Token"
    case appVersion = ""
}
