//
//  NewsExtension.swift
//  NYTimes
//
//  Created by Admin on 2/13/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

extension NewsViewController {
    func fetchNewsData() {
        self.startRegresher()
        api.getNewsListForSection(section: "/all-sections", period: "/7.json", completionHandler: { response in
            guard let data = response.data else {
                self.stopRefresher()
                return
            }

            self.parseCodable(data: data, type: News.self, successHandler: { news in
                // Save response in core data
                if let results = news.results {
                    self.newsData = results
                }

                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    self.stopRefresher()
                }
            }, errorHandler: { error in
                self.showAlert(title: error, message: "", style: .alert)
                self.stopRefresher()
            })
        })
    }
}
