//
//  NewsViewController.swift
//  NYTimes
//
//  Created by Admin on 2/13/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Kingfisher
import UIKit

class NewsViewController: BaseViewController {
    @IBOutlet var collectionView: UICollectionView!
    var refresher: UIRefreshControl!
    
    // Number of items per row to display
    let numberOfItemsPerRow: CGFloat = 2
    
    // Items line spacing padding
    let itemsPadding: CGFloat = 8
    
    var newsData: [Results] = [Results]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setupView() {
        self.title = "NewYork Times"
        self.refresher = UIRefreshControl()
        self.collectionView!.alwaysBounceVertical = true
        self.refresher.tintColor = UIColor.black
        self.activityIndicator.startAnimating()
        self.fetchNewsData()
    }
    
    @objc func loadData() {
        // code to execute during refresher
        self.fetchNewsData()
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    func startRegresher() {
        self.refresher.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        self.collectionView!.addSubview(self.refresher)
    }
}

extension NewsViewController {
    func registerCells() {}
    
    func getNumberOfSections() -> Int {
        return 1
    }
    
    func getNumberOfItemsInSection(section: Int) -> Int {
        return self.newsData.count
    }
    
    func getCell(indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}

extension NewsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let newsDetailsVC = storyboard?.instantiateViewController(withIdentifier: StoryboardIdentifier.NewsDetailsViewController) as? NewsDetailsViewController {
            newsDetailsVC.selectedItem = self.newsData[indexPath.row]
            self.navigationController?.pushViewController(newsDetailsVC, animated: true)
        }
    }
}

extension NewsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.getNumberOfSections()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.getNumberOfItemsInSection(section: section)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.NewsCollectionViewCell, for: indexPath) as? NewsCollectionViewCell {
            let newsItem = self.newsData[indexPath.row]
            
            if let strUrl = newsItem.media?.first?.media_metadata?.first?.url,
                let url = URL(string: strUrl) {
                self.newsData[indexPath.row]._imageUrl = strUrl
                cell.newsImage.kf.setImage(with: url)
            }
            if let title = newsItem.type {
                cell.newsLabel.text = title
            }
            if let date = newsItem.published_date {
                cell.dateLabel.text = date
            }
            
            return cell
        }
        return UICollectionViewCell()
    }
}

extension NewsViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let itemWidth = (collectionView.bounds.width / numberOfItemsPerRow) - itemsPadding
        return CGSize(width: itemWidth, height: itemWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return self.itemsPadding
    }
}
