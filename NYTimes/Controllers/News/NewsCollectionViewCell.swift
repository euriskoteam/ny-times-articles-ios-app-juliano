//
//  NewsollectionViewCell.swift
//  NYTimes
//
//  Created by Admin on 2/13/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var newsImage: UIImageView!
    @IBOutlet var newsLabel: UILabel!
}
