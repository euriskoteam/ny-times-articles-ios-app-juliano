//
//  NewsDetailsViewController.swift
//  NYTimes
//
//  Created by Admin on 2/13/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Kingfisher
import UIKit

class NewsDetailsViewController: UIViewController {
    var selectedItem: Results!
    
    @IBOutlet var imageNews: UIImageView!
    @IBOutlet var textLabelNews: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView() {
        if let strUrl = selectedItem._imageUrl,
            let url = URL(string: strUrl) {
            imageNews.kf.setImage(with: url)
            textLabelNews.text = selectedItem.title
        }
    }
}
