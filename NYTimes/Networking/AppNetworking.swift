//
//  AppNetworking.swift
//  Saradar
//
//  Created by Eurisko on 4/12/18.
//  Copyright © 2018 Saradar. All rights reserved.
//

import Alamofire
import UIKit

class AppNetworking {
    // Shared instance
    static let sharedInstance = AppNetworking()
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")

    // MARK: - Start Network Reachability

    func startNetworkReachabilityObserver() {
        if let baseVC = currentVC as? BaseViewController {
            reachabilityManager?.listener = { status in
                switch status {
                case .notReachable:
                    baseVC.showAlert(message: "No Internet Connection", style: .alert)
                case .unknown:
                    baseVC.showAlert(message: "It is unknown whether the network is reachable", style: .alert)
                case .reachable(.ethernetOrWiFi):
                    baseVC.showAlert(message: "The network is reachable over the WiFi connection", style: .alert)
                case .reachable(.wwan):
                    baseVC.showAlert(message: "The network is reachable over the WWAN connection", style: .alert)
                }
            }
        }
        reachabilityManager?.startListening()
        // start listening
    }

    // MARK: - Check connection

    func isConnected() -> Bool {
        if NetworkReachabilityManager()!.isReachable {
            return true
        } else {
            if let baseVC = currentVC as? BaseViewController {
                baseVC.showAlert(message: "No Internet Connection", style: .alert)
            }
            return false
        }
    }
}
