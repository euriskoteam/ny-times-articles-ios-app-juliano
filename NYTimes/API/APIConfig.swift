//
//  APIConfig.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation

struct APIConfig {
    // MARK: - API Config Shared Instance

    static let shared = APIConfig()
    private init() {}

    // MARK: - API Session

    let manager: URLSession = {
        let configuration = URLSessionConfiguration.default
        configuration.requestCachePolicy = .reloadIgnoringLocalCacheData
        configuration.timeoutIntervalForRequest = 60
        return URLSession(configuration: configuration)
    }()

    // MARK: - API BaseUrl

    private var _BaseUrl = "http://api.nytimes.com" /* REPLACE IT WITH YOUR BASE URL */
    var BaseUrl: String {
        get {
            return _BaseUrl
        }
        set {
            _BaseUrl = newValue
        }
    }

    // MARK: - ApiKey

    private var _APIKey: String = "?api-key=Rk6GfVjL9XA3A5ipo7bjr2fNh80CpeA5"
    var ApiKey: String {
        get {
            return _APIKey
        }
        set {
            _APIKey = newValue
        }
    }
}

// MARK: - API HTTP Request Content Type

struct ContentType {
    static let json = "application/json"
    static let formURLEncoded = "application/x-www-form-urlencoded"
}

// MARK: - API HTTP Request Methods

enum HTTPMethod: String {
    case connect = "CONNECT"
    case delete = "DELETE"
    case get = "GET"
    case head = "HEAD"
    case options = "OPTIONS"
    case patch = "PATCH"
    case post = "POST"
    case put = "PUT"
    case trace = "TRACE"
}
