//
//  APIEndPoints.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

// MARK: API Endpoints

struct APIEndPoints {
    static let fetchNews = "/svc/mostpopular/v2/mostviewed"
}
