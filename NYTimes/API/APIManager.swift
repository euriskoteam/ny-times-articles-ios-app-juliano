//
//  APIManager.swift
//  NYTimes
//
//  Created by Eurisko on 2/12/19.
//  Copyright © 2019 Eurisko Mobility. All rights reserved.
//

import Foundation

// MARK: - API HTTP Parameters

typealias Parameters = [String: Any]

// MARK: - API HTTP Headers

typealias HTTPHeaders = [String: Any]

// MARK: - API CompletionHandler

typealias CompletionHandler = (_ response: ResponseData) -> Void

// MARK: - API Manager

struct APIManager {
    // MARK: API Manager Shared Instance
    
    static let shared = APIManager()
    
    // MARK: API Private Init
    
    private init() {
        // coreapi.timeoutInterval = 60
    }
    
    // MARK: - API SERVER REQUEST
    
    private func makeHttpRequest(endPoint: String, httpMethod: HTTPMethod, parameters: Parameters? = nil, headers: HTTPHeaders? = nil, completionHandler: @escaping CompletionHandler) {
        var responseData = ResponseData()
        
        // Check Internet Connection
        if !AppNetworking.init().isConnected() {
            responseData.status = ResponseStatus.conenctionTimeout.rawValue
            responseData.message = ResponseMessage.connectionTimeout
            completionHandler(responseData)
            return
        }
        
        // Check URL
        guard let url = URL(string: "\(coreapi.BaseUrl)\(endPoint)\(coreapi.ApiKey)") else {
            responseData.status = ResponseStatus.failure.rawValue
            responseData.message = ResponseMessage.serverUnreachable
            completionHandler(responseData)
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = httpMethod.rawValue
        
        if let httpHeaders = headers as? [String: String] {
            urlRequest.allHTTPHeaderFields = httpHeaders
        }
        
        if let httpBody = parameters {
            do {
                urlRequest.httpBody = try NSKeyedArchiver.archivedData(withRootObject: httpBody, requiringSecureCoding: false)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        }
        
        let task = coreapi.manager.dataTask(with: urlRequest) {
            data, _, error in
            
            DispatchQueue.main.async(execute: {
                guard error == nil else {
                    responseData.status = ResponseStatus.failure.rawValue
                    responseData.message = error?.localizedDescription ?? ResponseMessage.serverUnreachable
                    completionHandler(responseData)
                    return
                }
                
                guard let data = data else {
                    responseData.status = ResponseStatus.failure.rawValue
                    responseData.message = ResponseMessage.serverUnreachable
                    completionHandler(responseData)
                    return
                }
                
                responseData.data = data
                completionHandler(responseData)
            })
        }
        task.resume()
    }
}

// MARK: - API Manager Extension

extension APIManager {
    /******************** API Calls ********************/
    static func getNewsAPIUrl(period: Int) -> String {
        switch period {
        case 1:
            return "/svc/mostpopular/v2/mostviewed/all-sections/1.json"
        case 7:
            return "/svc/mostpopular/v2/mostviewed/all-sections/7.json"
        case 30:
            return "/svc/mostpopular/v2/mostviewed/all-sections/30.json"
        default:
            break
        }
        return "/svc/mostpopular/v2/mostviewed/all-sections/7.json"
    }
    
    // MARK: - --------------------- 'GET' NEWS API CALL ---------------------
    
    func getNewsListForSection(section: String, period: String, completionHandler: @escaping CompletionHandler) {
        let endPoint = APIManager.getNewsAPIUrl(period: 7)
        self.makeHttpRequest(endPoint: endPoint, httpMethod: .get, parameters: nil, headers: nil) { data in
            completionHandler(data)
        }
    }
}
