SwiftLint:
- A tool to enforce Swift style and conventions,
- Installed using CocoaPod
- Adding SwiftFormat for Xcode Extension to Format the Entire File or Selected Source

UNIT Tests with XCTest:
- Using XCTestExpectation to Test Asynchronous Operations

CocoaPods:
- Installing Libraries using Pod Command Line
-  'Fabric' and 'Crashlytics' for App Crashes
- 'Alamofire', '~> 4.0.0' can be used for Elegant HTTP Request, Connectivity Checking
- 'UIEmptyState' used in case of Empty Collection View
- 'SwiftLint' used to enfore Swift Style
- 'Kingfisher' used to download image and save it in cache in case of Navigation

Main Concern:
- Application Structure: MVC
- ViewControllers subClassing using Extensions, Inherit Super Class(BaseViewController)
- Enumerations offer an easy way to work with sets of related constants 
