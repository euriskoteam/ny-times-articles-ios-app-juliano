//
//  BundleExtension.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation

extension Bundle {
    var displayName: String {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? ""
    }
    
    var targetName: String {
        return object(forInfoDictionaryKey: "CFBundleName") as? String ?? ""
    }
}
