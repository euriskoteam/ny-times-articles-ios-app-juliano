//
//  CollectionExtension.swift
//  NYTimes
//
//  Created by Admin on 2/12/19.
//  Copyright © 2019 eurisko. All rights reserved.
//

import Foundation

extension Collection where Indices.Iterator.Element == Index {
    subscript (exist index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
